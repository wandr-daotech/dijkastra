
const addStartVertex = (vertex) => {
  return {
    type: 'ADD_START_VERTEX',
    payload: vertex
  }
}

const addFinishVertex = (vertex) => {
  return {
    type: 'ADD_FINSIH_VERTEX',
    payload: vertex
  }
}

export default {
  addStartVertex,
  addFinishVertex
};
