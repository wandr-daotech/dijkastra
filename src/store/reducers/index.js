
const initialState = {
  StartVertex: 'A',
  FinishVertex: 'A'
}

const reducer = (state = initialState, action) => {

  switch (action.type) {
    case 'ADD_START_VERTEX':
      return {
        ...state,
        StartVertex: action.payload
      };
    case 'ADD_FINISH_VERTEX':
      return {
        ...state,
        FinishVertex: action.payload
      };
    default:
      return state;
  }
};


export default reducer;
