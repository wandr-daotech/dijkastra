import React from 'react';
import Graph from '../../utils/dijkstra';
import { connect } from 'react-redux';
import data from '../../test/graph2.json';

const commonKeys = Object.keys(data);
let g;
g = new Graph();
commonKeys.forEach((key) => {
  g.addVertex(key, data[key]);
});


const printShortPath = (graph, start, finish) => {
  const [shortestPath, distancesPath] = graph.shortestPath(start, finish);
  return `The shortest way from point ${start} to point ${finish} - ${shortestPath.concat([start]).reverse()} is full weight of the ${distancesPath}.`;
};



const Result = ({ startVertex, finishVertex }) => {
  return (
    <span className="alert alert-success"> {printShortPath(g, startVertex, finishVertex)} </span>
  );
};

const mapStateToProps = (state) => {
  return {
    startVertex: state.StartVertex,
    finishVertex: state.FinishVertex
  }
}

export default connect(mapStateToProps)(Result);
