import React, {Component} from 'react';
import { Provider } from 'react-redux';
import StartVertex from '../start-vertex';
import FinishVertex from '../finish-vertex';
import Result from '../result';

import store from '../../store/store';
import './app.css';


export default class App extends Component {

  render () {
    return (
      <div className="dijkstra-app">
        <Provider store = {store}>
      <div className="top-panel d-flex">
        <StartVertex />
        <FinishVertex />
      </div>
        <Result />
        </Provider>
      </div>
    );
  };
}
