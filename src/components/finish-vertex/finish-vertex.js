import React, { Component } from 'react';
import { connect } from 'react-redux';
import data from '../../test/graph2.json';
const commonKeys = Object.keys(data);


class FinishVertex extends Component {

  onLabelChange = (evt) => {
    this.props.addFinishVertex(evt.target.value);
  }


render() {
  return (
    <div>
      <select className = "custom-select"
      onChange={ this.onLabelChange } >{
        commonKeys.map(value =>
          <option value = {value} key = {value} >{value}</option>
        )};
      </select>
      </div>
    );
  };
}

const mapStateToProps = (state) => {
  return {
    FinishVertex: state.FinishVertex
  }
};


const mapDispatchToProps = (dispatch) => {
  return {
    addFinishVertex: (newVertex) => {
      dispatch({
        type: 'ADD_FINISH_VERTEX',
        payload: newVertex
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FinishVertex);
