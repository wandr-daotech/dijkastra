import React, { Component } from 'react';
import { connect } from 'react-redux';
import data from '../../test/graph2.json';
const commonKeys = Object.keys(data);


class StartVertex extends Component {

  onLabelChange = (evt) => {
    this.props.addStartVertex(evt.target.value);
  }

render() {
  return (
    <div>
      <select className = "custom-select"
      onChange={ this.onLabelChange }>{
        commonKeys.map(value =>
          <option value = {value} key = {value} >{value}</option>
        )};
      </select>
      </div>
    );
  };
}

const mapStateToProps = (state) => {
  return {
    StartVertex: state.StartVertex
  }
};


const mapDispatchToProps = (dispatch) => {
  return {
    addStartVertex: (newVertex) => {
      dispatch({
        type: 'ADD_START_VERTEX',
        payload: newVertex
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(StartVertex);
