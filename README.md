##"Dijkstra's algorithm
"Dijkstra's algorithm, conceived by Dutch computer scientist Edsger Dijkstra, is a graph search algorithm that solves the  
single-source shortest path problem for a graph with nonnegative edge path costs, producing a shortest path tree. This algorithm  
is often used in routing and as a subroutine in other graph algorithms."

## Available Scripts

In the project directory, you can run:

## `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


